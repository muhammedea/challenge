import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchBoxComponent } from '../search-box/search-box.component';
import { SearchServiceService } from '../search-service/search-service.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  declarations: [SearchBoxComponent],
  providers: [SearchServiceService],
  exports: [SearchBoxComponent]
})
export class SearchModule { }
