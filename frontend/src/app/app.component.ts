import { Component } from '@angular/core';
import { MainMenuService } from "./main-menu/main-menu.service";
import { CarouselImagesService } from "./carousel-images/carousel-images.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  menuItems = [];
  carouselImages = [];
  constructor(menuService: MainMenuService, carouselImagesService: CarouselImagesService) {
    menuService.getItems().then((items) => {
      this.menuItems = items;
    });
    carouselImagesService.getImages().then((images) => {
      this.carouselImages = images;
    });
  }
}
