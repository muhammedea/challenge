import { TestBed, inject } from '@angular/core/testing';

import { CarouselImagesService } from './carousel-images.service';

describe('CarouselImagesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CarouselImagesService]
    });
  });

  it('should be created', inject([CarouselImagesService], (service: CarouselImagesService) => {
    expect(service).toBeTruthy();
  }));

  it('should return Promise', inject([CarouselImagesService], (service: CarouselImagesService) => {
    const imgArray = service.getImages();
    expect(typeof (imgArray.then)).toBe('function');
  }));
});
