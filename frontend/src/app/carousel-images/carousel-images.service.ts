import { Injectable } from '@angular/core';

@Injectable()
export class CarouselImagesService {

  constructor() { }

  getImages() {
    let images = [
      "assets/carousel/img1.jpg",
      "assets/carousel/img2.jpg",
      "assets/carousel/img3.jpg",
      "assets/carousel/img4.jpg",
    ];
    return Promise.resolve(images);
  }

}
