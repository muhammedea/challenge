import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class SearchServiceService {

  private isLoading$: Observable<boolean>;
  private isLoadingObserver;

  constructor(private http: HttpClient) {
    this.isLoading$ = Observable.create((observer) => {
        this.isLoadingObserver = observer;
    })
  }


  /**
   * This function takes keywords observable and returns results observable
   * 
   * @param {Observable<string>} keywords$ 
   * @returns 
   * @memberof SearchServiceService
   */
  searchWithObservable (keywords$: Observable<string>) {
    const inputEvents$ = keywords$
            .filter(keyword => keyword.length > 2) // filter out small words
            .debounceTime(300)  //wait for user to wait 300 ms. this is for making requst count small
            .distinctUntilChanged();  //if same values come in a row, don't trigger.
    
    //this subscription runs before making calls
    inputEvents$.subscribe(() => this.isLoadingObserver.next(true) );

    // switchMap function takes keywords observable and creates new observable for each of them. 
    // if new item comes in, disposes the previous observable. This way the request will canceled.
    const responses$ = inputEvents$.switchMap(keyword => this.makeRequest(keyword), );
    
    //this results observable is a proxy ob
    const results$ = Observable.create((observer) => {

        //this subscription runs after a response has come
        responses$.subscribe((results) => {
            observer.next(results);
             this.isLoadingObserver.next(false);
        });

    });
    
    return results$;
  }


  /**
   * Makes backend request
   * 
   * @private
   * @param {string} keyword 
   * @returns Observable
   * @memberof SearchServiceService
   */
  private makeRequest (keyword: string) {
    return this.http.get<Array<string>>('http://localhost:8001/search?keyword=' + keyword);
  }

  getLoadingObservable () {
    return this.isLoading$;
  }

}
