import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainMenuService } from "./main-menu/main-menu.service";
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { CarouselImagesService } from "./carousel-images/carousel-images.service";
import { SearchModule } from "./search/search.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CarouselModule.forRoot(),
    SearchModule
  ],
  providers: [MainMenuService, CarouselImagesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
