import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { SearchServiceService } from "../search-service/search-service.service";
import "rxjs/Rx";
import { Subject } from "rxjs/Subject";

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss']
})
export class SearchBoxComponent implements OnInit {

  @ViewChild('searchInput') inputEl:ElementRef;
  results: Array<string> = [];
  keywords$ = new Subject<string>();
  isLoading: boolean = false;
  isInputFocused: boolean = false;

  constructor(private searchService: SearchServiceService) { }

  ngOnInit() {
    // searchWithObservable takes keyword observable and returns search result observable
    this.searchService.searchWithObservable(this.keywords$)
            .subscribe(results => this.results = results);
    // getLoadingObservable returns an observable that notifies when the loading status has changed
    this.searchService.getLoadingObservable()
            .subscribe(value =>  this.isLoading = value);
  }

  // when input changes it feeds the keywords observable
  onInput($event) {
    this.keywords$.next($event.target.value);
  }

  //function called when input focused
  onInputFocus() {
    this.isInputFocused = true;
  }
  //function called when input looses focus
  onInputBlur() {
    this.isInputFocused = false;
  }

  
  /**
   * This function determines whether or not the container result box should be visible
   * 
   * @returns boolean
   * @memberof SearchBoxComponent
   */
  isResultsBoxVisible() {
    if ( ! this.isInputFocused ) {
      return false;
    }
    return this.isResultsVisible() || this.isLoading;
  }

  
  
  /**
   * This function determines whether or not the result box should be visible
   * 
   * @returns 
   * @memberof SearchBoxComponent
   */
  isResultsVisible() {
    return this.inputEl.nativeElement.value.length > 2 && this.results.length > 0 && !this.isLoading;
  }

}
