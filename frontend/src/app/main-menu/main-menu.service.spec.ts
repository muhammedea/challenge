import { TestBed, inject } from '@angular/core/testing';

import { MainMenuService } from './main-menu.service';

describe('MainMenuService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MainMenuService]
    });
  });

  it('should be created', inject([MainMenuService], (service: MainMenuService) => {
    expect(service).toBeTruthy();
  }));

  it('should be created', inject([MainMenuService], (service: MainMenuService) => {
    expect(service).toBeTruthy();
  }));

  it('should return Promise', inject([MainMenuService], (service: MainMenuService) => {
    const items = service.getItems();
    expect(typeof (items.then)).toBe('function');
  }));
});
