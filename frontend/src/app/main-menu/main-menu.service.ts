import { Injectable } from '@angular/core';

@Injectable()
export class MainMenuService {

  constructor() { }

  getItems() {
    // This is an example. In reality it must be an backend call
    let items = [
      {name: "Clothing", link: ''},
      {name: "Travel", link: ''},
      {name: "Nursery Furniture", link: ''},
      {name: "Nursery Interiors", link: ''},
      {name: "Playtime", link: ''},
      {name: "Bathtime", link: ''},
      {name: "Feeding", link: ''},
      {name: "Gifts", link: ''}
    ];
    return Promise.resolve(items);
  }

}
