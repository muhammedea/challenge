webpackJsonp([1],{

/***/ "./src async recursive":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "./src async recursive";

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n    <div class=\"row\"><!-- header -->\n        <div class=\"col-xs-2 hidden-md hidden-lg hamburger-container\">\n          <div class=\"icon-menu\"></div><!-- hamburger -->\n        </div>\n        <div class=\"col-md-2 col-xs-8 col-md-offset-5 header-logo\">\n          <div class=\"icon-logo\"></div><!-- main logo -->\n        </div>\n        <div class=\"col-xs-2 hidden-md hidden-lg bag-container\">\n            <div class=\"icon-bag\"></div><!-- bag icon in mobile -->\n        </div>\n        <div class=\"col-md-5 col-xs-6 col-xs-offset-3 col-md-offset-0\">\n            <div class=\"top-menu hidden-xs hidden-sm\"><!-- desktop top menu -->\n                <div class=\"top-menu--item\">Sign in / Register</div>\n                <div class=\"top-menu--item\">Stores / Stockists</div>\n                <div class=\"top-menu--item\">Your Bag (2)</div>\n            </div>\n            <div class=\"top-menu hidden-md hidden-lg\"><!-- mobile top menu -->\n                <div class=\"top-menu--item\">Sign in / Register</div>\n                <div class=\"top-menu--item\">Stores</div>\n            </div>\n        </div>\n    </div><!-- header -->\n    <div class=\"row\">\n        <div class=\"col-md-12 hidden-xs hidden-sm menu\">\n            <div *ngFor=\"let menuItem of menuItems\" class=\"menu--item\">\n              {{ menuItem.name | uppercase }}\n            </div>\n        </div>\n    </div>\n    <div class=\"row search-box-container\">\n      <div class=\"col-md-4 col-xs-12 col-md-offset-4\">\n          <app-search-box></app-search-box>\n      </div>\n    </div>\n    <div class=\"image-carousel\">\n        <carousel>\n            <slide *ngFor=\"let carouselImage of carouselImages\">\n                <img [src]=\"carouselImage\" alt=\"\">\n            </slide>\n        </carousel>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/app.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".top-menu {\n  margin-top: 22px;\n  font-size: 13px; }\n  .top-menu--item {\n    display: inline-block;\n    padding: 0 10px; }\n    .top-menu--item:not(:last-child) {\n      border-right: 1px black solid; }\n  @media (min-width: 960px) {\n    .top-menu {\n      text-align: right; } }\n  @media (max-width: 960px) {\n    .top-menu {\n      text-align: center; } }\n\n.menu {\n  font-family: 'Ropa Sans', sans-serif;\n  font-weight: bold;\n  padding-top: 22px;\n  font-size: 16px;\n  text-align: center; }\n  .menu--item {\n    display: inline-block;\n    padding: 0 15px; }\n\n.header-logo {\n  padding-top: 25px;\n  text-align: center; }\n\n.hamburger-container {\n  padding-top: 40px;\n  text-align: center; }\n\n.bag-container {\n  padding-top: 35px;\n  text-align: center; }\n\n.icon-logo, .icon-bag, .icon-menu, .icon-search-icon {\n  display: inline-block; }\n\n.search-box-container {\n  margin-top: 20px; }\n\n.image-carousel {\n  margin-top: 20px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__main_menu_main_menu_service__ = __webpack_require__("./src/app/main-menu/main-menu.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__carousel_images_carousel_images_service__ = __webpack_require__("./src/app/carousel-images/carousel-images.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = (function () {
    function AppComponent(menuService, carouselImagesService) {
        var _this = this;
        this.title = 'app';
        this.menuItems = [];
        this.carouselImages = [];
        menuService.getItems().then(function (items) {
            _this.menuItems = items;
        });
        carouselImagesService.getImages().then(function (images) {
            _this.carouselImages = images;
        });
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("./src/app/app.component.html"),
        styles: [__webpack_require__("./src/app/app.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__main_menu_main_menu_service__["a" /* MainMenuService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__main_menu_main_menu_service__["a" /* MainMenuService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__carousel_images_carousel_images_service__["a" /* CarouselImagesService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__carousel_images_carousel_images_service__["a" /* CarouselImagesService */]) === "function" && _b || Object])
], AppComponent);

var _a, _b;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__main_menu_main_menu_service__ = __webpack_require__("./src/app/main-menu/main-menu.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap_carousel__ = __webpack_require__("./node_modules/ngx-bootstrap/carousel/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__carousel_images_carousel_images_service__ = __webpack_require__("./src/app/carousel-images/carousel-images.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__search_search_module__ = __webpack_require__("./src/app/search/search.module.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap_carousel__["a" /* CarouselModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_6__search_search_module__["a" /* SearchModule */]
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_3__main_menu_main_menu_service__["a" /* MainMenuService */], __WEBPACK_IMPORTED_MODULE_5__carousel_images_carousel_images_service__["a" /* CarouselImagesService */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "./src/app/carousel-images/carousel-images.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarouselImagesService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CarouselImagesService = (function () {
    function CarouselImagesService() {
    }
    CarouselImagesService.prototype.getImages = function () {
        var images = [
            "assets/carousel/img1.jpg",
            "assets/carousel/img2.jpg",
            "assets/carousel/img3.jpg",
            "assets/carousel/img4.jpg",
        ];
        return Promise.resolve(images);
    };
    return CarouselImagesService;
}());
CarouselImagesService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], CarouselImagesService);

//# sourceMappingURL=carousel-images.service.js.map

/***/ }),

/***/ "./src/app/main-menu/main-menu.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainMenuService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MainMenuService = (function () {
    function MainMenuService() {
    }
    MainMenuService.prototype.getItems = function () {
        // This is an example. In reality it must be an backend call
        var items = [
            { name: "Clothing", link: '' },
            { name: "Travel", link: '' },
            { name: "Nursery Furniture", link: '' },
            { name: "Nursery Interiors", link: '' },
            { name: "Playtime", link: '' },
            { name: "Bathtime", link: '' },
            { name: "Feeding", link: '' },
            { name: "Gifts", link: '' }
        ];
        return Promise.resolve(items);
    };
    return MainMenuService;
}());
MainMenuService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], MainMenuService);

//# sourceMappingURL=main-menu.service.js.map

/***/ }),

/***/ "./src/app/search-box/search-box.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"search-box\">\n  <input #searchInput (input)=\"onInput($event)\" (focus)=\"onInputFocus()\" (blur)=\"onInputBlur()\" placeholder=\"Hello, I'm looking for...\" >\n  <div class=\"icon-search-icon\"></div>\n  <div class=\"results-box\" [ngClass]=\"{'results-box__active': isResultsBoxVisible()}\">\n    <div *ngIf=\"isLoading\" class=\"loading-text\">Loading...</div>\n    <ul *ngIf=\"isResultsVisible()\">\n        <li *ngFor=\"let result of results\">\n            {{ result }}\n        </li>\n    </ul>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/search-box/search-box.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".search-box {\n  width: 100%;\n  display: inline-block;\n  position: relative; }\n\ninput {\n  width: 100%;\n  font-size: 16px;\n  height: 33px;\n  line-height: 33px;\n  outline: none;\n  border: none;\n  border-bottom: 1px solid black;\n  font-style: italic; }\n\n.results-box {\n  display: none;\n  position: absolute;\n  top: 32px;\n  border: 1px solid black;\n  z-index: 1000;\n  width: 100%;\n  background-color: white;\n  min-height: 100px;\n  max-height: 160px;\n  overflow-y: auto; }\n  .results-box__active {\n    display: block; }\n\n.icon-search-icon {\n  position: absolute;\n  right: 10px;\n  top: 10px; }\n\nul {\n  padding: 10px 20px;\n  margin: 0;\n  width: 100%; }\n\nli {\n  list-style: none;\n  font-weight: bold;\n  padding: 6px; }\n\n.loading-text {\n  height: 100px;\n  line-height: 100px;\n  text-align: center; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "./src/app/search-box/search-box.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__search_service_search_service_service__ = __webpack_require__("./src/app/search-service/search-service.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__ = __webpack_require__("./node_modules/rxjs/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchBoxComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SearchBoxComponent = (function () {
    function SearchBoxComponent(searchService) {
        this.searchService = searchService;
        this.results = [];
        this.keywords$ = new __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__["Subject"]();
        this.isLoading = false;
        this.isInputFocused = false;
    }
    SearchBoxComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.searchService.searchWithObservable(this.keywords$)
            .subscribe(function (results) { return _this.results = results; });
        this.searchService.getLoadingObservable()
            .subscribe(function (value) { return _this.isLoading = value; });
    };
    SearchBoxComponent.prototype.onInput = function ($event) {
        this.keywords$.next($event.target.value);
    };
    SearchBoxComponent.prototype.onInputFocus = function () {
        this.isInputFocused = true;
    };
    SearchBoxComponent.prototype.onInputBlur = function () {
        this.isInputFocused = false;
    };
    SearchBoxComponent.prototype.isResultsBoxVisible = function () {
        if (!this.isInputFocused) {
            return false;
        }
        return this.isResultsVisible() || this.isLoading;
    };
    SearchBoxComponent.prototype.isResultsVisible = function () {
        return this.inputEl.nativeElement.value.length > 2 && this.results.length > 0 && !this.isLoading;
    };
    return SearchBoxComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* ViewChild */])('searchInput'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* ElementRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* ElementRef */]) === "function" && _a || Object)
], SearchBoxComponent.prototype, "inputEl", void 0);
SearchBoxComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Component */])({
        selector: 'app-search-box',
        template: __webpack_require__("./src/app/search-box/search-box.component.html"),
        styles: [__webpack_require__("./src/app/search-box/search-box.component.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__search_service_search_service_service__["a" /* SearchServiceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__search_service_search_service_service__["a" /* SearchServiceService */]) === "function" && _b || Object])
], SearchBoxComponent);

var _a, _b;
//# sourceMappingURL=search-box.component.js.map

/***/ }),

/***/ "./src/app/search-service/search-service.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/@angular/common/http.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchServiceService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SearchServiceService = (function () {
    function SearchServiceService(http) {
        var _this = this;
        this.http = http;
        this.isLoading$ = __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].create(function (observer) {
            _this.isLoadingObserver = observer;
        });
    }
    SearchServiceService.prototype.searchWithObservable = function (keywords$) {
        var _this = this;
        var inputEvents$ = keywords$
            .filter(function (keyword) { return keyword.length > 2; }) // filter out small words
            .debounceTime(300) //wait for user to wait 300 ms. this is for making requst count small
            .distinctUntilChanged(); //if same values come in a row, don't trigger.
        //this subscription runs before making calls
        inputEvents$.subscribe(function () { return _this.isLoadingObserver.next(true); });
        var responses$ = inputEvents$.switchMap(function (keyword) { return _this.makeRequest(keyword); });
        //this results observable is a proxy ob
        var results$ = __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].create(function (observer) {
            //this subscription runs after a response has come
            responses$.subscribe(function (results) {
                observer.next(results);
                _this.isLoadingObserver.next(false);
            });
        });
        return results$;
    };
    SearchServiceService.prototype.makeRequest = function (keyword) {
        return this.http.get('http://localhost:8001/search?keyword=' + keyword);
    };
    SearchServiceService.prototype.getLoadingObservable = function () {
        console.log('call');
        return this.isLoading$;
    };
    return SearchServiceService;
}());
SearchServiceService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClient */]) === "function" && _a || Object])
], SearchServiceService);

var _a;
//# sourceMappingURL=search-service.service.js.map

/***/ }),

/***/ "./src/app/search/search.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search_box_search_box_component__ = __webpack_require__("./src/app/search-box/search-box.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__search_service_search_service_service__ = __webpack_require__("./src/app/search-service/search-service.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/@angular/common/http.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var SearchModule = (function () {
    function SearchModule() {
    }
    return SearchModule;
}());
SearchModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["b" /* NgModule */])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["a" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClientModule */]
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_2__search_box_search_box_component__["a" /* SearchBoxComponent */]],
        providers: [__WEBPACK_IMPORTED_MODULE_3__search_service_search_service_service__["a" /* SearchServiceService */]],
        exports: [__WEBPACK_IMPORTED_MODULE_2__search_box_search_box_component__["a" /* SearchBoxComponent */]]
    })
], SearchModule);

//# sourceMappingURL=search.module.js.map

/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map