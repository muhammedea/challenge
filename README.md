# Chellange Project

This is an example elasticsearch based web app. It has developed with docker. It can easly installable and deployable.
For runing the project. Just use the command below:

    $ docker-compose up

After containers are ready. Go to the URL below:

    $ http://localhost:8000


## Project structure
This project consists of 4 containers. Every folder in this project is a Dockerfile containing folder.

- **frontend:** This is an angular project. 

- **api:** This is the end point the frontend is talking to. it creates an elasticsearch query and sends it to the queue (rabbitmq)

- **rabbitmq:** This queue is the middle point for all the backend services. (microservice)

- **searchservice:** This container contains elasticsearch, some initial data and a microservice that takes query messages from queue and sends it to the elasticsearch instace.
