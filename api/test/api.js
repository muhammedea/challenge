//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();


chai.use(chaiHttp);

//Our parent block
describe('API', () => {

	/*
	* Test for default rout
	*/
    describe('/ route', () => {
        it('it should return default message', (done) => {
			chai.request(server)
		    .get('/')
		    .end((err, res) => {
			  	res.should.have.status(200);
			  	res.body.should.be.a('string');
			  	res.body.should.be.eql("Service is running");
		        done();
		    });
        });
    });

	/*
	* Test search with empty word
	*/
	describe('search with empty word', () => {
		it('it should return empty array', (done) => {
			chai.request(server)
			.get('/search?keyword=')
			.end((err, res) => {
				res.should.have.status(200);
				res.body.should.be.a('array');
				res.body.length.should.be.eql(0);
				done();
			});
		});
	});

	/*
	* Test search with small words (length <= 2)
	*/
	describe('search with small words', () => {
		it('it should return empty array', (done) => {
			chai.request(server)
			.get('/search?keyword=a')
			.end((err, res) => {
				res.should.have.status(200);
				res.body.should.be.a('array');
				res.body.length.should.be.eql(0);
				done();
			});
		});
	});

	/*
	* Test search with small words (length <= 2)
	*/
	describe('search with small words', () => {
		it('it should return empty array', (done) => {
			chai.request(server)
			.get('/search?keyword=co')
			.end((err, res) => {
				res.should.have.status(200);
				res.body.should.be.a('array');
				res.body.length.should.be.eql(0);
				done();
			});
		});
	});

	/*
	* Test fuzzy search
	*/
	describe('search with "pushchrs"', () => {
		it('it should return pushchairs containing elements', (done) => {
			chai.request(server)
			.get('/search?keyword=pushchrs')
			.end((err, res) => {
				res.should.have.status(200);
				res.body.should.be.a('array');
				res.body.length.should.be.above(0);
				done();
			});
		});
	});

	/*
	* Test exact search
	*/
	describe('search with "207552314"', () => {
		it('it should return one element', (done) => {
			chai.request(server)
			.get('/search?keyword=207552314')
			.end((err, res) => {
				res.should.have.status(200);
				res.body.should.be.a('array');
				res.body.length.should.be.eql(1);
				done();
			});
		});
	});

});
  