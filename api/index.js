const express = require('express');
const app = express();
const routes = require('./lib/routes');

//This is only for development puposes. For allowing CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//Routes
app.use(routes);


const server = app.listen(8001);