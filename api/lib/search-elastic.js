"use strict";
/**
 * SearchElastic
 */

const MicroserviceKit = require('microservice-kit');
//This is for shared queue instance
let searchQueue = null;


/**
 * MicroserviceKit is for comunicatin with rabbitmq 
 */
const microserviceKit = new MicroserviceKit({
    type: 'query-maker',
    config: null,
    amqp: {
    	url: "amqp://rabbitmq:rabbitmq@rabbitmq:5672",
        queues: [
            {
                name: 'search',
                key: 'search',
                options: { durable: true }
            }
        ],
        logger: function() {
            let args = Array.prototype.slice.call(arguments);
            args.unshift('[amqpkit]');
            console.log.apply(console, args);
        }
    }
});

/**
 * İnitialize microservice and get the queue for search
 */
microserviceKit
    .init()
    .then(() => {
        searchQueue = microserviceKit.amqpKit.getQueue('search');
    })
    .catch((err) => {
        console.log('Cannot boot');
        console.log(err.stack);
    });



/**
 * This function sends query object to rabbitmq queue for elasticsearch
 * This uses searchQueue object initialized by microserviceKit above
 * 
 * @param {object} query  elasticsearch query object
 * @returns Promise
 */
function sendQueryToQueue(query) {
	if ( ! searchQueue ) {
		return Promise.reject();
	}
    
    return searchQueue.sendEvent('search.job', query, {persistent: true});
}


/**
 * This function takes a keyword and creates a elasticsearch query object
 * 
 * @param {string} keyword 
 * @returns {object}
 */
function prepareQuery(keyword) {
	return {
		"query": {
			"bool" : {
				"should" :   [
					{ "fuzzy": { "name": keyword } }, //use fuzzy search for name
					{ "fuzzy": { "description": keyword } }, //use fuzzy search for description
					{ "term" : { "sku" : keyword } },  //use exact match for sku
					{ "term" : { "ediRef" : keyword } }  //use exact match for ediRef
				]
			}
		},
		"size": 10,  //limits the result count
		"sort": { "isInStock": { "order": "desc" } }  //this is for making isInStock elements top element
	};
}


/**
 * This is the main function for this service
 * It takes keyword and prepares query and sends it to the queue and returns the result as promise.
 * 
 * @param {string} keyword 
 * @returns Promise
 */
function SearchElastic(keyword) {
	return new Promise((resolve, reject) => {
		const query = prepareQuery(keyword);
		sendQueryToQueue(query)
		    .then((response) => {
				//just take the names of the search results
				const names = response.hits.hits.map(hit => hit._source.name);
		    	resolve(names);
		    })
		    .catch(reject);
	});
}


module.exports = SearchElastic;