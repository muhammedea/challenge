"use strict";

const express = require('express');
const router = express.Router();
const searchElastic = require('./search-elastic');


//for debugging. log every request
router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now());
    next();
});

// this is default url. This can be used for servie health check
router.get('/', function(req, res) {
    res.send('Service is running');
});

// This is the actual route that we use for search
// It reads keyword from query string. than calls the searchElastic function.
router.get('/search', function(req, res) {
    const keyword = req.query.keyword || '';
    if (keyword.length > 2) {
  	    searchElastic(keyword).then(result => res.send(result));
    } else {
  	    res.status(400).send('Bad Request');
    }
});


module.exports = router;