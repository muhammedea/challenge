# API service

This service is a REST API build with express.js. And it uses the microservice-kit library. 

$ /search?keyword={keyword}

Example request URL is shown above. It creates elasticsearch query and sends it to the queue.