# Search Service

This container consists of two parts.

## Elasticsearch Server

This docker image is drived from the official elasticsearch image.

## Elasticsearch Gateway Service

This is a node.js service that uses the microservice-kit library 
and forwards the query requests that it has taken from th queue
to the elasticsearch server.


### Sidenote

Puting multiple services in one container is not an ideal solution. 
But it has been done for the purpose of this challenge.