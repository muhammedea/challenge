"use strict";

const mappings = {
	properties: {
		'sku': {
			'type': 'string',
			'index': 'not_analyzed',
		},
		'ediRef': {
			'type': 'string',
			'index': 'not_analyzed'
		},
		'name': {
			'type': 'string',
		},
		'description': {
			'type': 'string'
		},
		'isInStock': {
			'type': 'boolean'
		},
	}
};

function initialize(client) {
	return new Promise((resolve, reject) => {
		client.indices.create({index: 'site'})
		.then(() => {
			console.log('created');
			client.indices.putMapping({  
				index: 'site',
				type: 'product',
				body: mappings
			}).then(() => {
				console.log('mappings');
				const products = require('./products.json');
				
				let operations = products.map((product) => {
					return [
						{ index:  { _index: 'site', _type: 'product' } },
						product
					];
				});
				operations = [].concat.apply([], operations);

				client.bulk({
					body: operations
				})
				.then(resolve)
				.catch((err) => {
					console.log('bulk error', err);
					reject();
				});
				
			}).catch((err) => {
				console.log('error mappings', err);
				reject();
			});
		})
		.catch((err) => {
			console.log('error', err);
			reject();
		});
	});
}


function initializeIfNecessary(client) {
	return new Promise((resolve, reject) => {
		client.indices.exists({index:"site"})
		.then((resp) => {
			console.log('exists', resp);
			//client.indices.delete({index:"site"});
			if (resp) {
				resolve();
			} else {
				initialize(client).then(resolve);
			}
		}).catch((err) => {
			console.log('error exists', err);
			reject(err);
		});
	});
}

module.exports = {initializeIfNecessary};