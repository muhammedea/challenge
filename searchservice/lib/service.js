"use strict";

const MicroserviceKit = require('microservice-kit');

function startMicroService(client) {
    const Errors = MicroserviceKit.Errors;


    const microserviceKit = new MicroserviceKit({
        type: 'search-worker',
        config: null, 
        amqp: {
            url: "amqp://rabbitmq:rabbitmq@rabbitmq:5672",
            queues: [{
                name: 'search',
                key: 'search',
                options: { durable: true }
            }],
            logger: function() {
                let args = Array.prototype.slice.call(arguments);
                args.unshift('[amqpkit]');
                console.log.apply(console, args);
            }
        },
        shutdown: {
            logger: function() {
                let args = Array.prototype.slice.call(arguments);
                args.unshift('[shutdownkit]');
                console.log.apply(console, args);
            }
        }
    });


    microserviceKit
        .init()
        .then(() => {
            // Run phase
            console.log("Waiting for search object.");

            const searchQueue = microserviceKit.amqpKit.getQueue('search');

            searchQueue.consumeEvent('search.job', (data, callback, progress, routingKey) => {
                console.log("Received: " + JSON.stringify(data));

                client.search({  
                	index: 'site',
                	type: 'product',
                	body: data
                })
                .then(result => callback(null, result))
                .catch(() => callback(new Errors.ClientError('search error occured')));

            });
        })
        .catch((err) => {
            console.log('Cannot start service');
            console.log(err.stack);
        });
}


module.exports = {startMicroService};