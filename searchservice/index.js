"use strict";
const client = require('./lib/esclient');
const initialize = require('./lib/initialize');
const service = require('./lib/service');

console.log('search microservice started');

initialize.initializeIfNecessary(client)
.then(() => {
	console.log('data initialize')
	service.startMicroService(client);
})
.catch(() => {
	console.log("errorrrrr");
});